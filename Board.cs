using System;
using System.Drawing;

namespace Reversi
{
    public class Board
    {

		public const int Empty = 0;
		public const int Black = 1;
		public const int White = 2;

        public int[,] Stones;
        public int Columns, Rows;

        public Board() : this(6, 6) { }

        public Board(int w, int h)
        {
            Columns = w;
            Rows = h;
            Stones = new int[Columns, Rows];
            Stones[Columns / 2, Rows / 2] = White;
            Stones[Columns / 2 - 1, Rows / 2 - 1] = White;
            Stones[Columns / 2 - 1, Rows / 2] = Black;
            Stones[Columns / 2, Rows / 2 - 1] = Black;
        }

        public int BlackStoneCount()
        {
            int result = 0;
            for (int i = 0; i < Columns; i++)
                for (int j = 0; j < Rows; j++)
                    if (Stones[i, j] != Empty)
                        if (Stones[i, j] == Black)
                            result++;
            return result;
        }

        public int WhiteStoneCount()
        {
            int result = 0;
            for (int i = 0; i < Columns; i++)
                for (int j = 0; j < Rows; j++)
                    if (Stones[i, j] != Empty)
                        if (Stones[i, j] == White)
                            result++;
            return result;
        }

        public bool IsFull()
        {
            for (int i = 0; i < Columns; i++)
                for (int j = 0; j < Rows; j++)
                    if (Stones[i, j] == Empty)
                        return false;
            return true;
        }

        // Returns whether stone can be placed
        public bool CanMove(int stone)
        {
            for (int i = 0; i < Columns; i++)
                for (int j = 0; j < Rows; j++)
                    if (TryPlaceStone(i, j, stone))
                        return true;
            return false;
        }

        // Used to clone a board
        public Board(Board b)
        {
            Columns = b.Columns;
            Rows = b.Rows;
            Stones = b.Stones;
        }

        // Returns true on succes, false on failure
        public bool PlaceStone(int x, int y, int stone)
        {
            if (x >= Columns || x < 0 || y >= Rows || y < 0)
            {
                Console.WriteLine("ERROR: Trying to place stone out of bounds: {" + x + ", " + y + "}");
                return false;
            }

            if (Stones[x, y] != Empty)
            {
                // Can't do that! Square is already filled!
                return false;
            }
            else
            {
                if (FlipStones(x, y, stone))
                {
                    Stones[x, y] = stone;
                    return true;
                }
                return false;
            }
        }

        /*
         * See if you can place a stone, but don't actually place it.
         *
         * Returns true if the stone can be placed, otherwise false
         */
        public bool TryPlaceStone(int x, int y, int stone)
        {
            if (x >= Columns || x < 0 || y >= Rows || y < 0)
            {
                return false;
            }

            if (Stones[x, y] != Empty)
            {
                return false;
            }
            else
            {
                if (TryFlipStones(x, y, stone))
                {
                    return true;
                }
                return false;
            }
        }

        public bool TryFlipStones(int x, int y, int color)
        {
            bool flippedSomething = false;
            flippedSomething |= tryFlip(x - 1, y - 1, -1, -1, color, true);
            flippedSomething |= tryFlip(x, y - 1, 0, -1, color, true);
            flippedSomething |= tryFlip(x + 1, y - 1, 1, -1, color, true);
            flippedSomething |= tryFlip(x - 1, y, -1, 0, color, true);
            flippedSomething |= tryFlip(x + 1, y, +1, 0, color, true);
            flippedSomething |= tryFlip(x - 1, y + 1, -1, 1, color, true);
            flippedSomething |= tryFlip(x, y + 1, 0, 1, color, true);
            flippedSomething |= tryFlip(x + 1, y + 1, 1, 1, color, true);
            return flippedSomething;
        }

        public bool FlipStones(int x, int y, int color)
        {
            bool flippedSomething = false;
            flippedSomething |= flip(x - 1, y - 1, -1, -1, color, true);
            flippedSomething |= flip(x, y - 1, 0, -1, color, true);
            flippedSomething |= flip(x + 1, y - 1, 1, -1, color, true);
            flippedSomething |= flip(x - 1, y, -1, 0, color, true);
            flippedSomething |= flip(x + 1, y, +1, 0, color, true);
            flippedSomething |= flip(x - 1, y + 1, -1, 1, color, true);
            flippedSomething |= flip(x, y + 1, 0, 1, color, true);
            flippedSomething |= flip(x + 1, y + 1, 1, 1, color, true);
            return flippedSomething;
        }

        /*
         * Recursive function to try to flip stones.
         *
         * Normally, recursive functions are a bad way to solve problems. But in
         * this case, I don't believe there is a better way to solve this problem :/
         */
        bool tryFlip(int x, int y, int dx, int dy, int color, bool first)
        {
            // Out of bounds
            if (x < 0 || x >= Columns || y < 0 || y >= Rows)
                return false;

            // Empty field
            if (Stones[x, y] == Empty)
                return false;

            // Found a equal color at the end of the row
            if (Stones[x, y] == color)
                return !first;

            // Recurse
            if (tryFlip(x + dx, y + dy, dx, dy, color, first &= false))
            {
                return true;
            }
            else
                return false;
        }

        /*
         * Recursive function to flip stones.
         *
         * Normally, recursive functions are a bad way to solve problems. But in
         * this case, I don't believe there is a better way to solve this problem :/
         */
        bool flip(int x, int y, int dx, int dy, int color, bool first)
        {
            // Out of bounds
            if (x < 0 || x >= Columns || y < 0 || y >= Rows)
                return false;

            // Empty field
            if (Stones[x, y] == Empty)
                return false;

            // Found a equal color at the end of the row
            if (Stones[x, y] == color)
                return !first;

            // Recurse
            if (flip(x + dx, y + dy, dx, dy, color, first &= false))
            {
                Stones[x, y] = color;
                return true;
            }
            else
                return false;
        }
    }
}


