using System;
using System.Windows.Forms;
using System.Drawing;
using System.Text;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Reversi
{
    public class Game : Form
    {

		/*************/
		/* Prologue */
		/***********/

		public static void Main()
		{
			Application.Run(new Game());
		}

		/**************************/
		/* Chapter 1: Game rules */
		/************************/

		public const int Empty = 0;
		public const int Black = 1;
		public const int White = 2;

		// Only P2 can be a cpu (no cpu-vs-cpu)
		public bool CpuP2 = false;

		public int WhosAtTurn;

		Board board = new Board();


		public void restart()
		{
			board = new Board(board.Columns, board.Rows);
			bitm = new Bitmap(formWidth, formHeight);
			WhosAtTurn = Black;
            HelpP1 = false;
            HelpP2 = false;
			UpdateScreen();
		}

		public void CheckTurn()
		{
			if(board.IsFull()) {
				GameOver ();
				return;
			}

			if(board.CanMove(WhosAtTurn)) {
				if(WhosAtTurn == White && CpuP2) {
					CpuTurn();
				}
			} else {
				if(!board.CanMove(Next(WhosAtTurn))) {
					GameOver();
					return;
				}

				// Skip turn
				WhosAtTurn = Next(WhosAtTurn);
				if(Networked)
					NetworkCommand("PASS\n");

				// A CpuTurn may be neccessary
				CpuTurn();
			}
			UpdateScreen();
		}

		public static int Next(int stone)
		{
			return stone == White ? Black : White;
		}

		/*
		 * Checks whether the cpu is at turn, and if so, do something
		 */
		public void CpuTurn()
		{
			if(WhosAtTurn == White && CpuP2) {

				// If no legal move is possible, skip
				if(!board.CanMove(WhosAtTurn)) {
					WhosAtTurn = Next(WhosAtTurn);
					UpdatelabelWhosAtTurn();
					CheckTurn();
					return;
				}

				// Delay by 1/4th of a second
				new Thread(() => {
					Thread.Sleep(250);
					/*
					 * The AI is rather stupid atm, it picks the first legal move.
					 * However, it is remarkable easy to lose to it, so I think it
					 * suits this game very well.
					 */
					for (int i = 0; i < board.Columns; i++)
						for (int j = 0; j < board.Rows; j++)
						if(board.PlaceStone(i, j, WhosAtTurn)) {
							WhosAtTurn = Next(WhosAtTurn);
							CheckTurn();
							return;
						}

					// Couldnt place something => skip turn
					WhosAtTurn = Next(WhosAtTurn);

					CheckTurn();
				}).Start();
			}
		}

		public void GameOver()
		{
			UpdateScreen();
            GameEndedForm = new Form();

            GameEndedForm.StartPosition = FormStartPosition.CenterScreen;
            GameEndedForm.Size = new Size(212, 212);
            GameEndedForm.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            
            Label winnerName = new Label();
            winnerName.AutoSize = true;
            winnerName.Font = myFont;

            Label scoreTitle = new Label();
            scoreTitle.AutoSize = true;
            scoreTitle.Font = myFont;
            scoreTitle.Text = "Score:";
            scoreTitle.Location = new Point(0, 90);

            PictureBox winnerPic = new PictureBox();
            winnerPic.Size = new Size(50, 50);

            PictureBox scoreP1 = new PictureBox();
            scoreP1.Size = new Size(50, 50);
            scoreP1.Image = pictureScoreBlack.Image;

            PictureBox scoreP2 = new PictureBox();
            scoreP2.Size = new Size(50, 50);
            scoreP2.Image = pictureScoreWhite.Image;

            PictureBox restart = new PictureBox();
            restart.Size = new Size(50, 50);
            restart.Image = Properties.Resources.repeat.ToBitmap();
            restart.Click += endedGameRestart;

            int black = board.BlackStoneCount();
            int white = board.WhiteStoneCount();

            if (black > white)
            {
                winnerName.Text = NameP1 + "\nHas won!";
                winnerPic.Image = BmpStone(Colors[1], pictureWhosAtTurn.Size.Width,
                                               pictureWhosAtTurn.Size.Height, ""); ;
            }
                if (white > black)
                {
                    winnerName.Text = NameP2 + "\nHas won!";
                    winnerPic.Image = BmpStone(Colors[2], pictureWhosAtTurn.Size.Width,
                                               pictureWhosAtTurn.Size.Height, ""); ;
                }
                if (black == white)
                {
                    winnerName.Text = "It's a tie!";
                }

            GameEndedForm.Controls.Add(winnerName);

            if (winnerName.Width + 70 > 412)
                GameEndedForm.Width = winnerName.Width + 70;
            winnerPic.Location = new Point(winnerName.Width, 16);
            scoreP1.Location = new Point(0, 130);
            scoreP2.Location = new Point(50, 130);
            restart.Location = new Point(140, 114);

            GameEndedForm.Controls.Add(winnerPic);
            GameEndedForm.Controls.Add(scoreP1);
            GameEndedForm.Controls.Add(scoreP2);
            GameEndedForm.Controls.Add(scoreTitle);
            GameEndedForm.Controls.Add(restart);

            
            GameEndedForm.ShowDialog();
            GameEndedForm.Refresh();
            GameEndedForm.Deactivate += resetGameEndedForm;

		}

		/******************************/
		/* Chapter 2: User Interface */
		/****************************/

        Form GameEndedForm = new Form();

		// How much of the square is filled with the stone
		public double Diameter = 0.9;
		// tiltCam angle: [0,90] 90: See stones on top, 0: See stones on side
		double tiltCam = 65;

		// Default colors: {Empty, P1, P2}
		public Color[] Colors = {
			Color.Transparent,
			Color.FromArgb(44, 190, 168),
			Color.FromArgb(211, 65, 87) };

		Color gridColor = Color.Black;
		Color fieldBackColor = Color.FromArgb(255, 255, 150);

		// Help is turned off by default
		public bool HelpP1 = false;
		public bool HelpP2 = false;
        public bool helpWasClicked = false;

		// The font used everywhre
		Font myFont = new Font("Arial", 16);
		int formWidth = 700, formHeight = 600;
		int controlRuimteX = 160, controlRuimteY = 26;

		bool settingsAreChanged;

		Button passButton = new Button();

		MenuStrip menuStrip = new MenuStrip();
		ToolStripMenuItem menu = new ToolStripMenuItem("Game");
		ToolStripMenuItem help = new ToolStripMenuItem("Help");

		PictureBox field = new PictureBox();
		Bitmap bitm;

		PictureBox pictureScoreBlack = new PictureBox();
		PictureBox pictureScoreWhite = new PictureBox();
		PictureBox pictureWhosAtTurn = new PictureBox();

		Label labelWhosAtTurn = new Label();
		Label labelScoreTitle = new Label();
		Label labelScoreBlack = new Label();
		Label labelScoreWhite = new Label();

		// Used in the configuration
		TextBox inputNumberofRows = new TextBox();
		TextBox inputNumberofColumns = new TextBox();

        // SettingsFormControls
        VScrollBar alpha = new VScrollBar();
        TextBox nameBoxP1 = new TextBox();
        TextBox nameBoxP2 = new TextBox();
        Button buttonP1 = new Button();
        Button buttonP2 = new Button();
        Button buttonGrid = new Button();
        Button buttonBG = new Button();
        Label labelC = new Label(), labelR = new Label();
        Label labelr = new Label(), labelg = new Label(), labelb = new Label();
        Bitmap chooseColorBitmap = new Bitmap(180, 180);
        Bitmap cp_Pointer = new Bitmap(180, 180);
        PictureBox chooseColor = new PictureBox();
        PictureBox picked_ColorBox = new PictureBox();
        Color picked_Color;
        Pen blackPen = new Pen(Color.Black);
		public string NameP1 = "Player1";
		public string NameP2 = "Player2";
        public string PreviousNameP2 = "";
        int aCP = 0, rCP = 127, gCP = 127, bCP = 127;
        int arCP, agCP, abCP;


		public Game()
		{

			// Black always begins at Reversi
			WhosAtTurn = Black;

			bitm = new Bitmap(formWidth, formHeight);
			bitm.MakeTransparent();
			this.Icon = Properties.Resources.games2;
			this.ClientSize = new Size(formWidth + controlRuimteX, formHeight + controlRuimteY);
			this.Text = "Reversi by Sophie & Martijn";
			this.StartPosition = FormStartPosition.CenterScreen;
			this.BackColor = Color.LightGray;
			this.MinimumSize = new Size(400,400);

			Bitmap playIco = Properties.Resources.play.ToBitmap();
			Bitmap helpIco = Properties.Resources.help.ToBitmap();
			Bitmap settingsIco = Properties.Resources.process.ToBitmap();
            Bitmap computerIco = Properties.Resources.computer.ToBitmap();
            Bitmap netIco = Properties.Resources.wired.ToBitmap();

			inputNumberofColumns.Text = "6";
			inputNumberofRows.Text = "6";

			menu.DropDownItems.Add("Restart Game", playIco, restart_Clicked);
			menu.DropDownItems.Add("Play against CPU", computerIco, CpuModeP2_Clicked);
			menu.DropDownItems.Add("Network play (beta)", netIco, networkPlay_Clicked);
            menu.DropDownItems.Add("Settings", settingsIco, settings_Clicked);

			help.DropDownItems.Add ("Help (F1)", helpIco, help_Clicked);
			help.DropDownItems.Add("Helpmode " + NameP1, BmpStone(Colors[1], 32, 32, ""), helpModeP1_Clicked);
			help.DropDownItems.Add("Helpmode " + NameP2, BmpStone(Colors[2], 32, 32, ""), helpModeP2_Clicked);

			menu.BackColor = Color.Beige;
			menuStrip.BackColor = Color.Beige;
			menuStrip.Items.Add(menu);
			menuStrip.Items.Add(help);
			Controls.Add(menuStrip);
			this.MainMenuStrip = menuStrip;


			field.Size = new Size(formWidth, formHeight);
			field.Location = new Point(0, 0 + controlRuimteY);
			field.BackColor = fieldBackColor;
			Controls.Add(field);
			field.MouseClick += FieldClicked;

			labelWhosAtTurn.Location = new Point(formWidth + 5, formHeight + controlRuimteY - 90 - 20);
			labelWhosAtTurn.AutoSize = true;
			labelWhosAtTurn.Font = myFont;
			labelWhosAtTurn.Text = "At turn:";
			Controls.Add(labelWhosAtTurn);

            pictureWhosAtTurn.Location = new Point(formWidth + 100,
			                                       (int)(formHeight + controlRuimteY
			                                        - 97 - 50 * (90 - tiltCam) / 90));
			pictureWhosAtTurn.Size = new Size(50, 50);
			pictureWhosAtTurn.Image = BmpStone(Colors[WhosAtTurn], pictureWhosAtTurn.Size.Width,
                                               pictureWhosAtTurn.Size.Height, "");
			Controls.Add(pictureWhosAtTurn);

			labelScoreTitle.Location = new Point(formWidth + 5, formHeight + controlRuimteY - 315);
			labelScoreTitle.AutoSize = true;
            labelScoreTitle.Font = new Font("Arial", 16, FontStyle.Bold);
			labelScoreTitle.Text = "Score:";
			Controls.Add(labelScoreTitle);

			pictureScoreBlack.Location = new Point(formWidth, formHeight + controlRuimteY - 97 - 180);
			pictureScoreBlack.Size = new Size(50, 50);
			pictureScoreBlack.Image = BmpStone(Color.Black, pictureScoreBlack.Size.Width,
                                               pictureScoreBlack.Size.Height, board.BlackStoneCount() + "");
			Controls.Add(pictureScoreBlack);

			pictureScoreWhite.Location = new Point(formWidth, formHeight + controlRuimteY - 97 -120);
			pictureScoreWhite.Size = new Size(50, 50);
			pictureScoreWhite.Image = BmpStone(Color.White, pictureScoreWhite.Size.Width,
                                               pictureScoreWhite.Size.Height, board.WhiteStoneCount() + "");
			Controls.Add(pictureScoreWhite);

            labelScoreBlack.Location = new Point(formWidth + 50, formHeight + controlRuimteY - 265 - 7);
            labelScoreBlack.Size = new Size(controlRuimteX - 50, pictureScoreBlack.Size.Height);
            labelScoreBlack.Font = new Font("Courier New", 14);
            labelScoreBlack.Text = NameP1;
			Controls.Add(labelScoreBlack);

            labelScoreWhite.Location = new Point(formWidth + 50, formHeight + controlRuimteY - 205 - 7);
            labelScoreWhite.Size = new Size(controlRuimteX - 50, pictureScoreWhite.Size.Height);
            labelScoreWhite.Font = new Font("Courier New", 14);
            labelScoreWhite.Text = NameP2;
			Controls.Add(labelScoreWhite);

			passButton.Location = new Point(formWidth + 5, formHeight + controlRuimteY - 45);
			passButton.Font = myFont;
			passButton.Text = "Pass";
			passButton.AutoSize = true;
			passButton.Click += passButton_Clicked;
			Controls.Add(passButton);

			FormClosing += CloseWindow_Clicked;


			// We don't need this kind of this
			CheckForIllegalCrossThreadCalls = false;

			PaintScreen();
		}

		public void CloseWindow_Clicked(object o, EventArgs e) {
			if(networkThread != null)
				networkThread.Abort();

			if(client != null)
				client.Close();
		}

		public void restart(object o, System.EventArgs e)
		{
			if (settingsAreChanged) {

                if (!int.TryParse(inputNumberofColumns.Text, out board.Columns))
                    board.Columns = 6;
				if (board.Columns < 3)
					board.Columns = 3;
				inputNumberofColumns.Text = "" + board.Columns;

                if (!int.TryParse(inputNumberofRows.Text, out board.Rows))
                    board.Rows = 6;
				if (board.Rows < 3)
					board.Rows = 3;
				inputNumberofRows.Text = "" + board.Rows;

				DrawGrid();
				restart();
				if(Networked) {
					StartNetworkGame(false);
					NetworkCommand("RESIZE " + board.Columns + "," + board.Rows + "\n");
				}
			}
		}

        public void endedGameRestart(object o, EventArgs e)
        {
            GameEndedForm.Close();
            restart();
        }


		protected override void OnResizeEnd(EventArgs e)
		{
			formWidth = this.ClientSize.Width - 160;
			formHeight = this.ClientSize.Height - controlRuimteY;
			labelWhosAtTurn.Location = new Point(formWidth + 5, formHeight + controlRuimteY - 90);
			pictureWhosAtTurn.Location = new Point(formWidth + 100, formHeight + controlRuimteY - 97);
			passButton.Location = new Point(formWidth + 5, formHeight + controlRuimteY - 45);
			pictureScoreBlack.Location = new Point(formWidth, formHeight + controlRuimteY - 97 - 180);
			pictureScoreWhite.Location = new Point(formWidth, formHeight + controlRuimteY - 97 - 120);
            labelScoreBlack.Location = new Point(formWidth + 50, formHeight + controlRuimteY - 265 - ((10)));
            labelScoreWhite.Location = new Point(formWidth + 50, formHeight + controlRuimteY - 205 - ((10)));
			labelScoreTitle.Location = new Point(formWidth + 5, formHeight + controlRuimteY - 315);

			field.Size = new Size(formWidth, formHeight);
			bitm = new Bitmap(formWidth, formHeight);
			DrawGrid();
			DrawStones();

		}

        public void resetGameEndedForm(object o, EventArgs e)
        {
            GameEndedForm.Controls.Clear();
        }

		public void restart_Clicked(object o, System.EventArgs e)
		{
			if(Networked) {
				NetworkCommand("RESTART\n");
				StartNetworkGame(false);
			}

			restart();
		}

        public void settings_Clicked(object o, System.EventArgs e)
        {
            settingsAreChanged = false;
            Form settings = new Form();
            settings.Text = "Settings";
            settings.ShowInTaskbar = false;
            settings.Icon = Properties.Resources.process;
            settings.StartPosition = FormStartPosition.CenterScreen;
            settings.ClientSize = new Size(180 + 127, 50 + 180 + 100);
            settings.BackColor = Color.LightGray;
            settings.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            settings.FormClosed += checkIfSettingsChanged;
            settings.FormClosed += restart;


            Label labelSetColors = new Label();
            labelSetColors.Location = new Point(0, 3);
            labelSetColors.Font = new Font("Arial", 12, FontStyle.Bold);
            labelSetColors.Text = "Set Color:";
            labelSetColors.AutoSize = true;
            settings.Controls.Add(labelSetColors);


            labelC.Location = new Point(210, 30);
            labelC.Font = new Font("Arial", 12, FontStyle.Bold);
            labelC.Text = "Columns";
            settings.Controls.Add(labelC);

            inputNumberofColumns.Location = new Point(216, 60);
            inputNumberofColumns.Width = 80;
            settings.Controls.Add(inputNumberofColumns);


            labelR.Location = new Point(210, 90);
            labelR.Font = new Font("Arial", 12, FontStyle.Bold);
            labelR.Text = "Rows";
            settings.Controls.Add(labelR);

            inputNumberofRows.Location = new Point(216, 120);
            inputNumberofRows.Width = 80;
            settings.Controls.Add(inputNumberofRows);


            nameBoxP1.Location = new Point(0, 30);
            nameBoxP1.BackColor = Color.Gainsboro;
            nameBoxP1.Font = new Font("Arial", 12, FontStyle.Italic);
            nameBoxP1.Text = NameP1;
            nameBoxP1.AutoSize = true;
            settings.Controls.Add(nameBoxP1);

            buttonP1.Location = new Point(140, 30);
            buttonP1.BackColor = Colors[1];
            buttonP1.Width = 40;
            settings.Controls.Add(buttonP1);
            buttonP1.Click += setP1color;


            nameBoxP2.Location = new Point(0, 60);
            nameBoxP2.BackColor = Color.Gainsboro;
            nameBoxP2.Font = new Font("Arial", 12, FontStyle.Italic);
            nameBoxP2.Text = NameP2;
            nameBoxP2.AutoSize = true;
            settings.Controls.Add(nameBoxP2);

            buttonP2.Location = new Point(140, 60);
            buttonP2.BackColor = Colors[2];
            buttonP2.Width = 40;
            settings.Controls.Add(buttonP2);
            buttonP2.Click += setP2color;

            Label labelGrid = new Label();
            labelGrid.Location = new Point(0, 90);
            labelGrid.Font = new Font("Arial", 12, FontStyle.Italic);
            labelGrid.Text = "Grid";
            labelGrid.AutoSize = true;
            settings.Controls.Add(labelGrid);

            buttonGrid.Location = new Point(140, 90);
            buttonGrid.BackColor = gridColor;
            buttonGrid.Width = 40;
            settings.Controls.Add(buttonGrid);
            buttonGrid.Click += setGridcolor;


            Label labelBG = new Label();
            labelBG.Location = new Point(0, 120);
            labelBG.Font = new Font("Arial", 12, FontStyle.Italic);
            labelBG.Text = "Background";
            labelBG.AutoSize = true;
            settings.Controls.Add(labelBG);

            buttonBG.Location = new Point(140, 120);
            buttonBG.BackColor = fieldBackColor;
            buttonBG.Width = 40;
            settings.Controls.Add(buttonBG);
            buttonBG.Click += setBGcolor;


            chooseColor.Location = new Point(0, 150);
            chooseColor.Size = new Size(chooseColorBitmap.Width, chooseColorBitmap.Height);
            chooseColor.BackgroundImage = chooseColorBitmap;
            settings.Controls.Add(chooseColor);
            chooseColor.MouseMove += moveOnColor;
            chooseColor.MouseClick += clickOnColor;


            alpha.Location = new Point(183, 150);
            alpha.Height = 180;
            alpha.Width = 30;
            alpha.Minimum = -50;
            alpha.Maximum = 59;
            alpha.Value = aCP;
            alpha.ValueChanged += alpha_Changed;
            settings.Controls.Add(alpha);

            Button resetAlpha = new Button();
            resetAlpha.Location = new Point(216, 227);
            resetAlpha.AutoSize = true;
            resetAlpha.Text = "Reset Alpha";
            settings.Controls.Add(resetAlpha);
            resetAlpha.Click += reset_Alpha;


            picked_ColorBox.Location = new Point(216, 150);
            picked_ColorBox.Size = new Size(88, 75);
            settings.Controls.Add(picked_ColorBox);
            // Border
            Bitmap bFrame = new Bitmap(picked_ColorBox.Width, picked_ColorBox.Height);
            Graphics gFrame = Graphics.FromImage(bFrame);
            gFrame.DrawRectangle(blackPen, 0, 0, picked_ColorBox.Width - 1, picked_ColorBox.Height - 1);
            picked_ColorBox.Image = bFrame;


            labelr.Location = new Point(213, 255);
            labelr.Size = new Size(120, 25);
            labelr.Font = new Font("Arial", 13);
            settings.Controls.Add(labelr);

            labelg.Location = new Point(213, 280);
            labelg.Size = new Size(120, 25);
            labelg.Font = new Font("Arial", 13);
            settings.Controls.Add(labelg);

            labelb.Location = new Point(213, 305);
            labelb.Size = new Size(120, 25);
            labelb.Font = new Font("Arial", 13);
            settings.Controls.Add(labelb);

            drawColorPickSpectrum();

            // Border
            Graphics ccFrame = Graphics.FromImage(chooseColorBitmap);
            ccFrame.DrawRectangle(blackPen, 0, 0, chooseColorBitmap.Width, chooseColorBitmap.Height);
            update_CP_Values();
            settings.ShowDialog();
        }

        public void drawColorPickSpectrum()
        {
            // |||||||||Color Picker Spectrum|||||||||
            // ColorPicker Width w, Height h
            double w = 180, h = 180;
            // Pixelbreedte Kleurkolom p, RGB Coefficient 255/Pixelbreedte Kleurkolom d
            double p = w / 6, d = 255 / p;
            // Aantal Kleurkolommen is 6
            int[] kleurWaarden = new int[6];


            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    double dx = x, dy = y;
                    kleurWaarden[1] = (int)(255 - d * (x % p) + (127 - (255 - d * (x % p))) * (h - dy) / h);
                    kleurWaarden[2] = (int)(0 + 127 * (h - dy) / h);
                    kleurWaarden[3] = (int)(0 + 127 * (h - dy) / h);
                    kleurWaarden[4] = (int)(d * (x % p) + (127 - d * (x % p)) * (h - dy) / h);
                    kleurWaarden[5] = (int)(255 + (127 - 255) * (h - dy) / h);
                    kleurWaarden[0] = (int)(255 + (127 - 255) * (h - dy) / h);

                    for (int k = 0; k < 6; k++)
                    {
                        if (x >= k * p && x < (k + 1) * p)
                        {
                            chooseColorBitmap.SetPixel((int)h - (x + 1), (int)h - (y + 1),
                                                       Color.FromArgb(kleurWaarden[k % 6],
                                                                      kleurWaarden[(k + 2) % 6],
                                                                      kleurWaarden[(k + 4) % 6]));
                        }
                    }
                }
            }
        }



        public void setP1color(object o, EventArgs e)
        {
            Colors[1] = Color.FromArgb(arCP, agCP, abCP);
            buttonP1.BackColor = Colors[1];
            pictureScoreBlack.Image = BmpStone(Colors[1], pictureScoreBlack.Size.Width,
			                                   pictureScoreBlack.Size.Height, isP2CPU());
            help.DropDownItems[1].Image = BmpStone(Colors[1], 32, 32, isP2CPU());
            UpdateScreen();
        }

        public void setP2color(object o, EventArgs e)
        {
            Colors[2] = Color.FromArgb(arCP, agCP, abCP);
            buttonP2.BackColor = Colors[2];
            pictureScoreWhite.Image = BmpStone(Colors[2], pictureScoreWhite.Size.Width,
			                                   pictureScoreWhite.Size.Height, isP2CPU());
            help.DropDownItems[2].Image = BmpStone(Colors[2], 32, 32, isP2CPU());
            UpdateScreen();
        }

        public void setGridcolor(object o, EventArgs e)
        {
            gridColor = Color.FromArgb(arCP, agCP, abCP);
            buttonGrid.BackColor = gridColor;
            UpdateScreen();
        }

        public void setBGcolor(object o, EventArgs e)
        {
            fieldBackColor = Color.FromArgb(arCP,agCP,abCP);
            buttonBG.BackColor = fieldBackColor;
            field.BackColor = fieldBackColor;
        }


        public void moveOnColor(object o, MouseEventArgs mea)
        {
            if ((Control.MouseButtons & MouseButtons.Left) != 0)
                change_Color();
        }

        public void clickOnColor(object o, EventArgs e)
        {
            change_Color();
        }

        public void change_Color()
        {
                int x = onInterval(0 + 1, chooseColorBitmap.Width - 1, chooseColor.PointToClient(MousePosition).X);
                int y = onInterval(0 + 1, chooseColorBitmap.Height - 1, chooseColor.PointToClient(MousePosition).Y);
                cp_Pointer = new Bitmap(chooseColorBitmap.Width, chooseColorBitmap.Height);
                Graphics ccFrame = Graphics.FromImage(cp_Pointer);

                ccFrame.DrawLine(blackPen, x - 2, y, x - 5, y);
                ccFrame.DrawLine(blackPen, x + 2, y, x + 5, y);
                ccFrame.DrawLine(blackPen, x, y - 2, x, y - 5);
                ccFrame.DrawLine(blackPen, x, y + 2, x, y + 5);

                rCP = chooseColorBitmap.GetPixel(x, y).R;
                gCP = chooseColorBitmap.GetPixel(x, y).G;
                bCP = chooseColorBitmap.GetPixel(x, y).B;
                chooseColor.Image = cp_Pointer;
                update_CP_Values();
        }

        public void alpha_Changed(object o, EventArgs e)
        {
            aCP = alpha.Value;
            update_CP_Values();
        }

        public void reset_Alpha(object o, EventArgs e)
        {
            alpha.Value = 0;
            update_CP_Values();
        }

        public void update_CP_Values()
        {
            aCP = alpha.Value;
            arCP = onInterval(0, 255, (int)(rCP - Math.Sign(aCP) *
			                                (127.0 + Math.Abs(127.0 - rCP)) / 49.0 * Math.Abs(aCP)));
            agCP = onInterval(0, 255, (int)(gCP - Math.Sign(aCP) *
			                                (127.0 + Math.Abs(127.0 - gCP)) / 49.0 * Math.Abs(aCP)));
            abCP = onInterval(0, 255, (int)(bCP - Math.Sign(aCP) *
			                                (127.0 + Math.Abs(127.0 - bCP)) / 49.0 * Math.Abs(aCP)));

            labelr.Text = "R: " + arCP;
            labelg.Text = "G: " + agCP;
            labelb.Text = "B: " + abCP;
            picked_Color = Color.FromArgb(arCP, agCP, abCP);
            picked_ColorBox.BackColor = picked_Color;
        }

        private int onInterval(int min, int max, int v)
        {
            int result;
            if (v <= max)
                if (v >= min)
                    result = v;
                else
                    result = min;
            else
                result = max;
            return result;
        }


        public void help_Clicked(object o, EventArgs e)
        {
            help_Clicked();
        } 

        public void help_Clicked()
        {
            if (helpWasClicked == false)
            {
                DrawHelpStones();
                helpWasClicked = true;
            }
            else
            {
                helpWasClicked = false;
                UpdateScreen();
            }

        }

		public void helpModeP1_Clicked(object o, EventArgs e) {
			HelpP1 = !HelpP1;
			if(WhosAtTurn == Black && helpWasClicked == false)
				DrawHelpStones();
            else
                if (helpWasClicked == true)
                {
                    helpWasClicked = false;
                    UpdateScreen();
                }
		}

		public void helpModeP2_Clicked(object o, EventArgs e) {
			HelpP2 = !HelpP2;
			if(WhosAtTurn == White && helpWasClicked == false)
				DrawHelpStones();
            else
                if (helpWasClicked == true)
                {
                    helpWasClicked = false;         
                    UpdateScreen();
                }
		}

		public void CpuModeP2_Clicked(object o, EventArgs e) {
			if(!Networked) {
				CpuP2 = !CpuP2;
				CpuTurn();
				pictureScoreWhite.Image = BmpStone(Colors[2],
				                                   pictureScoreWhite.Size.Width,
				                                   pictureScoreWhite.Size.Height,
				                                   isP2CPU());
				if (CpuP2)
				{
					PreviousNameP2 = NameP2;
					NameP2 = "Computer";
				}
				else
					NameP2 = PreviousNameP2;

				Font fontName2 = new Font("Courier New", onInterval(5, 14, (labelScoreWhite.Width - 12) / NameP2.Length));
				labelScoreWhite.Font = fontName2;
				labelScoreWhite.Location = new Point(formWidth + 50,
				                                     formHeight + controlRuimteY - 205 - (int)fontName2.SizeInPoints / 2);

				help.DropDownItems[2].Text = "Helpmode " + NameP2;
				labelScoreWhite.Text = NameP2;
				UpdateScreen();
			}
		}

		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			if (keyData == Keys.F1)
			{
                help_Clicked();
				return true;
			}

			return base.ProcessCmdKey(ref msg, keyData);
		}

		public int[] BoardToPixels(int col, int row)
		{
			int x = (int)(field.Width * (col + 0.5) / board.Columns);
			int y = (int)(field.Height * (row + 0.5) / board.Rows);

			int[] coords = { x, y };
			return coords;
		}

		public int[] PixelsToBoard(int x, int y)
		{
			int col = x * board.Columns / field.Width;
			int row = y * board.Rows / field.Height;
			int[] coords = { col, row };
			return coords;
		}

		public void FieldClicked(object o, MouseEventArgs me)
		{

			if(Networked) {
				// Prevent p1 from "accidentally" playing for p2
				if(WhosAtTurn == White)
					return;
			}

			int[] coords = PixelsToBoard(me.X, me.Y);
			if (board.PlaceStone(coords[0], coords[1], WhosAtTurn))
			{
				WhosAtTurn = Next(WhosAtTurn);

				CheckTurn();

				if(Networked)
					NetworkCommand("PLACE " + coords[0] + "," + coords[1] + "\n");
			}
		}

		// Thread-safe way to call PaintScreen
		public void UpdateScreen()
		{
			this.Invoke((MethodInvoker)delegate {
				PaintScreen();
			});
		}

		/*
		 * Prepare the screen for the player
		 */
		public void PaintScreen() {
			UpdatelabelWhosAtTurn();
            helpWasClicked = false;

			DrawGrid ();
			Graphics.FromImage(bitm).Clear(Color.Transparent);
			DrawStones();

			// 'cause the network thread can't do this
			if(Networked) {
				help.DropDownItems[2].Text = "Helpmode " + NameP2;
				labelScoreWhite.Text = NameP2;
			}

			if(WhosAtTurn == Black) {
				if(HelpP1)
					DrawHelpStones();
			} else {
				if(HelpP2)
					DrawHelpStones();
			}

			pictureScoreBlack.Image = BmpStone(Colors[1],
			                                   pictureScoreBlack.Size.Width,
			                                   pictureScoreBlack.Size.Height,
			                                   board.BlackStoneCount() + "");
			pictureScoreWhite.Image = BmpStone(Colors[2],
			                                   pictureScoreWhite.Size.Width,
			                                   pictureScoreWhite.Size.Height,
			                                   board.WhiteStoneCount() + "");
		}

		public void UpdatelabelWhosAtTurn()
		{
            string txt = "";

            if (WhosAtTurn == 2)
                txt = isP2CPU();

			pictureWhosAtTurn.Image = BmpStone(Colors[WhosAtTurn],
			                                   pictureWhosAtTurn.Size.Width,
			                                   pictureWhosAtTurn.Size.Height,
			                                   txt);
		}

		public void checkIfSettingsChanged(object o, System.EventArgs e)
		{
			if (inputNumberofColumns.Text != "" + board.Columns ||
			    inputNumberofRows.Text != "" + board.Rows)
				settingsAreChanged = true;
            if (nameBoxP1.Text != NameP1 || nameBoxP2.Text != NameP2)
            {
                NameP1 = nameBoxP1.Text;
				if(Networked) {
					NetworkCommand("NAME " + NameP1 + "\n");
				} else {
					NameP2 = nameBoxP2.Text;
				}
				help.DropDownItems[1].Text = "Helpmode " + NameP1;
				help.DropDownItems[2].Text = "Helpmode " + NameP2;


				Font fontName1 = new Font("Courier New", onInterval(5, 14,(labelScoreBlack.Width - 12) / NameP1.Length));
				labelScoreBlack.Font = fontName1;
				labelScoreBlack.Location = new Point(formWidth + 50,
				                                     formHeight + controlRuimteY - 265 - (int)fontName1.SizeInPoints / 2);
				labelScoreBlack.Text = NameP1;

				Font fontName2 = new Font("Courier New", onInterval(5, 14,(labelScoreWhite.Width - 12) / NameP2.Length));
				labelScoreWhite.Font = fontName2;
				labelScoreWhite.Location = new Point(formWidth + 50,
				                                     formHeight + controlRuimteY - 205 - (int)fontName2.SizeInPoints / 2);
				labelScoreWhite.Text = NameP2;

			}
		}

        public string isP2CPU()
        {
            if (CpuP2 == true)
                return "cpu";
            return "";

        }

		public void passButton_Clicked(object o, System.EventArgs e)
		{
			// Prevent Black from "accidentally" skipping White's turn
			if(WhosAtTurn == White && (Networked || CpuP2))
				return;

			if(Networked)
				NetworkCommand("PASS\n");

			WhosAtTurn = Next(WhosAtTurn);
			UpdatelabelWhosAtTurn();

			CpuTurn();
		}


		/************************/
		/* Chapter 3: Graphics */
		/**********************/
		public void DrawGrid()
		{

			Bitmap bm = new Bitmap(field.Width, field.Height);
			Graphics g = Graphics.FromImage(bm);
			Pen gridPen = new Pen(gridColor);

			// Horizontal lines
			for (int i = 0; i <= board.Rows; i++)
			{
					g.DrawLine(gridPen, 0, field.Height * i / board.Rows,
				           field.Width, field.Height * i / board.Rows);
			}

			// Vertical lines
			for (int i = 0; i <= board.Columns; i++)
			{
					g.DrawLine(gridPen, field.Width * i / board.Columns, 0,
				           field.Width * i / board.Columns, field.Height);
			}

			// Accounting for rounding errors and such
			g.DrawLine(gridPen, field.Width, 0, field.Width, field.Height);
			g.DrawLine(gridPen, 0, field.Height, field.Width, field.Height);

			field.BackgroundImage = bm;
		}

		public void DrawStones()
		{
			Graphics.FromImage(bitm).Clear(Color.Transparent);
			for (int i = 0; i < board.Columns; i++)
				for (int j = 0; j < board.Rows; j++)
			{
				DrawStone(i, j, board.Stones[i, j]);
			}
		}

		public void DrawHelpStones()
		{

            if (helpWasClicked == false)
            {
                for (int i = 0; i < board.Columns; i++)
                    for (int j = 0; j < board.Rows; j++)
                        if (board.TryPlaceStone(i, j, WhosAtTurn))
                            DrawStone(i, j, Color.FromArgb(40, Colors[WhosAtTurn]));
            }
            helpWasClicked = true;
		}

		// arguments are in columns/rows, not pixels!
		public void DrawStone(int col, int row, int stone)
		{

			if(stone != Empty)
				DrawStone(col, row, Colors[stone]);
		}

		// arguments are in columns/rows, not pixels!
		public void DrawStone(int col, int row, Color color)
		{
			Graphics g = Graphics.FromImage(bitm);

			int[] coords = BoardToPixels(col, row);
			int x = coords[0], y = coords[1];
			drawEllipse(g, x, y, (int)(field.Width / board.Columns * Diameter),
			            (int)(field.Height / board.Rows * Diameter), color);
			field.Image = bitm;
		}

		public Bitmap BmpStone(Color color, int width, int height, string txt)
		{
			Bitmap BmpStone = new Bitmap(width, height);
			Graphics g = Graphics.FromImage(BmpStone);
            height = (int)(height * tiltCam / 90.0);
			drawEllipse(g, width / 2, height / 2, (int)(width*Diameter),
			            (int)(height*Diameter), color);
			if (txt != "")
			{
				int fontsize = onInterval(0, (int)(20* tiltCam / 90.0), (width - 12) / txt.Length);
				SolidBrush myBrush = new SolidBrush(Color.FromArgb(255 - color.R, 255 - color.G, 255 - color.B));
				Font stoneFont = new Font("Courier New", fontsize, FontStyle.Bold);
				g.DrawString(txt, stoneFont, myBrush, width / 2 - 3 - fontsize*txt.Length/2,
				             height / 2 - fontsize -2);
			}
			BmpStone.MakeTransparent();

			return BmpStone;
		}

		// Draw an ellipse with (x,y) as middle, horizontal diameter xDiam, vertical diameter yDiam
		public void drawEllipse(Graphics g, int x, int y, int xDiam, int yDiam, Color color)
		{
            int rPen = onInterval(0, 255, (int)(color.R - (127.0 + Math.Abs(127.0 - color.R)) / 2));
            int gPen = onInterval(0, 255, (int)(color.G - (127.0 + Math.Abs(127.0 - color.G)) / 2));
            int bPen = onInterval(0, 255, (int)(color.B - (127.0 + Math.Abs(127.0 - color.B)) / 2));
            Pen pen = new Pen(Color.FromArgb(rPen, gPen, bPen), 3);
            Brush brush = new SolidBrush(color);

            yDiam = (int)(yDiam * tiltCam/90.0);

            int yTop = (int)(y - yDiam / 2 + yDiam*(90 - tiltCam) / 180);
            int yBottom = (int)(y - yDiam / 2 - yDiam*(90 - tiltCam) / 180);

            // Bottom
            g.FillEllipse(brush, x - xDiam / 2, yTop, xDiam, yDiam);
            g.DrawEllipse(pen, x - xDiam / 2, yTop, xDiam, yDiam);

            // Filling inside
            g.FillRectangle(brush, x - xDiam / 2, yBottom + yDiam / 2, xDiam, yTop - yBottom);
            
            // Top
            g.FillEllipse(brush, x - xDiam / 2, yBottom, xDiam, yDiam);
            g.DrawEllipse(pen, x - xDiam / 2, yBottom, xDiam, yDiam);

            // Sidelines
            g.DrawLine(pen, x - xDiam / 2, yTop + yDiam / 2, x - xDiam / 2, yBottom + yDiam / 2);
            g.DrawLine(pen, x +1 + xDiam / 2, yTop + yDiam / 2, x+1 + xDiam / 2, yBottom + yDiam / 2);

		}


		/***********************/
		/* Chapter 4: Netplay */
		/*********************/

		public bool Networked = false;

		Thread networkThread;
		TcpClient client;
		NetworkStream stream;
		TextBox ipInput;
		Form networkConfig;
		const int port = 54239;
		char[] seperators = {' ', ',', '\n'};
		public void networkPlay_Clicked(object o, EventArgs e) {

			board = new Board(6, 6);

			networkThread = new Thread(() => {
				try {

					TcpListener tcpListener = new TcpListener(IPAddress.Any, port);
					tcpListener.Start();
					TcpClient client = tcpListener.AcceptTcpClient();
					client.NoDelay = true;
					stream = client.GetStream();
					networkConfig.Close();
					StartNetworkGame(true);
				}
				catch {

					if(client != null)
						client.Close();
				}
			});
			networkThread.Start();

			networkConfig = new Form();
			networkConfig.ClientSize = new Size(200, 70 + 70);
            networkConfig.StartPosition = FormStartPosition.CenterParent;

			IPHostEntry iphe = Dns.GetHostEntry(Dns.GetHostName());
			String ip = iphe.AddressList[0].ToString();

            if (iphe.AddressList[0].AddressFamily != AddressFamily.InterNetwork)
            {
                foreach (IPAddress a in iphe.AddressList)
                {
                    if (a.AddressFamily == AddressFamily.InterNetwork)
                    {
                        ip = a.ToString();
                        break;
                    }
                }
            }

			Label ipDisplay = new Label();
			ipDisplay.Text = "Your IP is " + ip;
			ipDisplay.Size = new Size(195, 16);
			networkConfig.Controls.Add(ipDisplay);
			ipDisplay.Location = new Point(2,2);

            Label howtoOnline = new Label();
            howtoOnline.Font = new Font("Arial", 9);
            howtoOnline.Text = "Open this screen both clients and make one connect to the other's IP-adress.";
            howtoOnline.Size = new Size(195, 70);
            networkConfig.Controls.Add(howtoOnline);
            howtoOnline.Location = new Point(2, 45);

			ipInput = new TextBox();
			ipInput.Text = "reversi.beide.org";
			ipInput.Size = new Size(195,50);
			networkConfig.Controls.Add (ipInput);
			ipInput.Location = new Point(2, 20);

			Button startButton = new Button();
			startButton.Text = "Connect!";
			startButton.Click += networkGameButton_Clicked;

			networkConfig.Closed += networkConfig_Closed;

			networkConfig.AcceptButton = startButton;
			startButton.Size = new Size(95, 20);
            networkConfig.Controls.Add(startButton);
            startButton.Location = new Point(90, 115);

			networkConfig.ShowDialog();
    }

		/*
		 * Sends a command to the other player.
		 * Before using, make sure the game is networked!
		 *
		 * And remember to end with a \n
		 */
		public void NetworkCommand(String s)
		{
			byte[] command = ASCIIEncoding.ASCII.GetBytes(s);
			stream.Write(command, 0, command.Length);
		}

		public void networkConfig_Closed(object o, EventArgs e) {
			if(networkThread != null) {
				if(networkThread.IsAlive) {
					networkThread.Abort();
				}
			}
		}

		public void networkGameButton_Clicked(object o, EventArgs e)
		{
			try {
				client = new TcpClient(ipInput.Text, port);
			} catch {
				MessageBox.Show("IP invalid! Please try again");
				return;
			}

			networkThread.Abort();
			networkConfig.Close ();


			/*
			 * Swap the colors
			 * Both players are their own P1, but the client should have
			 * switched colors, so that ze is white.
			 */
			Colors = new Color[] {Colors[0], Colors[2], Colors[1]};
			client.NoDelay = true;
			stream = client.GetStream();
			StartNetworkGame(false);
		}

		public void StartNetworkGame(bool black)
		{
			CpuP2 = false;
			Networked = true;

			restart();

			if(black) {
				Colors = new Color[]{
					Color.Transparent,
					Color.FromArgb(44, 190, 168),
					Color.FromArgb(211, 65, 87) };
				WhosAtTurn = Black;
			} else {
				Colors = new Color[]{
					Color.Transparent,
					Color.FromArgb(211, 65, 87),
					Color.FromArgb(44, 190, 168)};

				WhosAtTurn = White;

				board.Stones[board.Columns / 2, board.
				             Rows / 2] = Black;
				board.Stones[board.Columns / 2 - 1, board.Rows / 2 - 1] = Black;
				board.Stones[board.Columns / 2 - 1, board.Rows / 2] = White;
				board.Stones[board.Columns / 2, board.Rows / 2 - 1] = White;
			}

			UpdateScreen();

		networkThread = new Thread(() => {

				StringBuilder builder = new StringBuilder();

				while(true) {
					byte b;
					try {
						b = (byte)stream.ReadByte();
					} catch {
						if(client != null)
							client.Close();
						return;
					}

					// Indicates end of stream
					if(b == 255) {
						if(client != null)
							client.Close();
						MessageBox.Show("Connection was lost");
						Networked = false;
						return;
					}

					builder.Append((char)b);

					// End of command
					if((char)b == '\n') {
						String command = builder.ToString();
						String[] parts = command.Split(seperators);

						/*
						 * Unknown commands are simply ignored. That should allow for
						 * eventual extension of the protocol while preserving
						 * backward-compatibility.
						 */
						switch(parts[0]) {
						case "PLACE":
							if(WhosAtTurn == White) {
								int x = int.Parse(parts[1]);
								int y = int.Parse(parts[2]);

								if(board.PlaceStone(x, y, WhosAtTurn)) {
									WhosAtTurn = Next(WhosAtTurn);
									UpdateScreen();
								} else
									Console.WriteLine("Remote gave invalid position: " + x + "," + y);
							} else
								Console.WriteLine("Remote tried to cheat!");
							break;
						case "PASS":
							if(WhosAtTurn == White) {
								WhosAtTurn = Next(WhosAtTurn);
								UpdateScreen();
							}
							break;
						case "RESTART":
							StartNetworkGame(true);
							Console.WriteLine("Got RESTARTed");
							// We return here, because a new thread is made in StartNetworkGame
							return;
						case "RESIZE":
							int w = int.Parse(parts[1]);
							int h = int.Parse(parts[2]);
							board = new Board(w, h);
							StartNetworkGame(true);
							// We return here, because a new thread is made in StartNetworkGame
							return;
						case "TEST":
							/*
							 * TESTs are ignored, but without the warning message used
							 * with unknown commands. TEST can be used to test whether
							 * the connection works.
							 */
							break;
						case "NAME":
							NameP2 = parts[1];
							break;
						default:
							Console.WriteLine("Unknown command: " + command);
							break;
						}

						builder.Clear();
					}
				}
			});
			networkThread.Start();
		}

	}
}

