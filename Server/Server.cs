using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace Server
{
	public class Server
	{
		public static void Main()
		{
			new Server();
		}

		public Server() {
			int port = 54239;
			TcpListener tcpListener = new TcpListener(IPAddress.Any, port);
			tcpListener.Start();

			// Used for making turns go right
			byte[] restart = {
				(byte)'R',
				(byte)'E',
				(byte)'S',
				(byte)'T',
				(byte)'A',
				(byte)'R',
				(byte)'T',
				(byte)'\n'};

			while(true) {
				byte[] test = {
					(byte)'T',
					(byte)'E',
					(byte)'S',
					(byte)'T',
					(byte)'\n'};

				bool gotClient1 = false;
				bool gotClient2 = false;
				TcpClient client1, client2;

				/*
				 * This code makes 2 connections to play a game.
				 *
				 * Also, it makes sure that both clients are still present (so
				 * noone will get matched up with somebody who already left).
				 */
				// Sorry for the repetition of code, but otherwise the compiler
				// is not sure whether client1 and client2 are assigned.
				client1 = tcpListener.AcceptTcpClient();
				client2 = tcpListener.AcceptTcpClient();
				while(!gotClient1 || !gotClient2) {
					gotClient1 = false;
					gotClient2 = false;

					try {
						client1.GetStream().Write(test, 0, test.Length);
						gotClient1 = true;
					} catch {
						client1 = tcpListener.AcceptTcpClient();
						// We just made the connection, so we can assume it works.
						gotClient1 = true;
					}

					try {
						client2.GetStream().Write(test, 0, test.Length);
						gotClient2 = true;
					} catch {
						client2 = tcpListener.AcceptTcpClient();
						// Same as above
						gotClient2 = true;
						// However, client1 may have already left.
						gotClient1 = false;
					}
				}

				/*
				 * Now, we got to empty the buffers, because otherwise
				 * bad things will happen.
				 */
				emptyStream(client1.GetStream());
				emptyStream(client2.GetStream());

				/*
				 * One might ask, why use a ThreadPool if you keep increasing
				 * maxthreads anyway. Well, that is because this way, threads
				 * will be reused, and will thus to some avail reduce the
				 * vunerability to DOS.
				 */
				int maxWorkers, maxCompletions;
				ThreadPool.GetMaxThreads(out maxWorkers, out maxCompletions);
				ThreadPool.SetMaxThreads(maxWorkers + 2, maxCompletions + 2);

				// Now we make 2 threads that will pipe traffic between the clients.
			ThreadPool.QueueUserWorkItem(delegate {

					client1.NoDelay = true;
					client2.NoDelay = true;

					// Introducing asymmetry (both were white, now p1 is black)
					client1.GetStream().Write(restart, 0, restart.Length);

					NetworkStream stream1 = client1.GetStream();
					NetworkStream stream2 = client2.GetStream();
					byte b = 0;
					try {
						// 255 indicates end-of-stream (or so it appears)
						while(b != 255) {
							// Doing this copying this way, instead of using a buffer
							// or a pre-made function, is done because we are worried
							// about latency, not about bandwidth
							stream2.WriteByte(b = (byte)stream1.ReadByte());
						}
					} catch {}
					try {
						client1.Close();
						client2.Close();
					} catch {}
					Console.WriteLine("Stopped piece 1");
				});
			ThreadPool.QueueUserWorkItem(delegate {
					NetworkStream stream1 = client1.GetStream();
					NetworkStream stream2 = client2.GetStream();
					byte b = 0;
					try {
						while(b != 255) {
							stream1.WriteByte(b = (byte)stream2.ReadByte());
						}
					} catch {}
					try {
						client1.Close();
						client2.Close();
					} catch {}
					Console.WriteLine("Stopped piece 2");
				});

				Console.WriteLine("Started a game");
			}
		}

		void emptyStream(NetworkStream stream) {
			byte[] buf = new byte[1024];
			while(stream.DataAvailable)
				stream.Read(buf, 0, buf.Length);
		}
	}
}